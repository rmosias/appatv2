package com.example.mosias.atv2.View;

import android.os.Bundle;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.RadioGroup;
import android.support.v7.app.AppCompatActivity;
import com.example.mosias.atv2.R;

import com.example.mosias.atv2.WebServices.content.Item;

public class Cadastro extends AppCompatActivity{
    // campos da do layout
    private TextInputLayout tilNome;
    private TextInputEditText etNome;
    private TextInputLayout tilEndereco;
    private TextInputEditText etEndereco;
    private TextInputLayout tilTelefone;
    private TextInputEditText etTelefone;
    private TextInputLayout tilDescricao;
    private TextInputEditText etDescricao;

    private Item item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        inicializaComponentes();
    }

    private void inicializaComponentes()
    {
        tilNome = findViewById(R.id.tilNome);
        etNome = findViewById(R.id.etNome);
        tilEndereco = findViewById(R.id.tilEndereco);
        etEndereco = findViewById(R.id.etEndereco);
        tilTelefone = findViewById(R.id.tilTelefone);
        etTelefone = findViewById(R.id.etTelefone);
        tilDescricao = findViewById(R.id.tilDescricao);
        etDescricao = findViewById(R.id.etDescricao);


        FloatingActionButton fabConfirmar = findViewById(R.id.fabConfirmar);
        FloatingActionButton fabDeletar = findViewById(R.id.fabDeletar);
        //
        etNome.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                tilNome.setError(null);
            }
        });
        etEndereco.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                tilEndereco.setError(null);
            }
        });
        etTelefone.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                tilTelefone.setError(null);
            }
        });
        etDescricao.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                tilDescricao.setError(null);
            }
        });

        //
        fabConfirmar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                confirmaTela();
            }
        });
        //
        fabDeletar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //deletaRegistro();
            }
        });
    }

    private boolean validaTela()
    {
        boolean retorno = true;
        //
        if (etNome.getText().toString().trim().length() == 0)
        {
            tilNome.setError("Informe o nome do local");
            retorno = false;
        }
        //
        if (etEndereco.getText().toString().trim().length() == 0)
        {
            tilEndereco.setError("Informe o endereco do local");
            retorno = false;
        }

        if (etTelefone.getText().toString().trim().length() < 10 || etTelefone.getText().toString().trim().length() > 10)
        {
            tilTelefone.setError("Telefone Invalido");
            retorno = false;
        }
        //
        if (etDescricao.getText().toString().trim().length() <20 )
        {
            tilDescricao.setError("A descricao deve conter 20 caracteres!");
            retorno = false;
        }
        //

        return retorno;
    }

    private void confirmaTela()
    {
        if (!validaTela())
            return;

        //salvaRegistro();
    }



}
