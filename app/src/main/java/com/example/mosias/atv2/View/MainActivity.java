package com.example.mosias.atv2.View;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mosias.atv2.R;
import com.example.mosias.atv2.WebServices.WebServiceController;
import com.example.mosias.atv2.WebServices.content.Item;
import com.example.mosias.atv2.WebServices.content.Squidex;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_REGISTRO = "com.example.mosias.atv2.View.MainActivity.EXTRA_REGISTRO";
    private ListView lvMapa;
    private SwipeRefreshLayout srMapa;
    private FloatingActionButton fabConfirmar;
    private Squidex listMapa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializaComponenetes();
        criaAdapterLista();
    }

    @Override
    protected void onResume() {
        super.onResume();
        carregarWebService();
    }

    private void inicializaComponenetes() {
        lvMapa = findViewById(R.id.lvMapa);
        srMapa = findViewById(R.id.srMapa);
        fabConfirmar = findViewById(R.id.fabConfirmar);
        srMapa.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                carregarWebService();
            }
        });
        //
        fabConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Cadastro.class));
            }
        });
        //
        lvMapa.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Item item = listMapa.getItems()[position];
                Intent intent = new Intent(MainActivity.this, Cadastro.class);
                intent.putExtra(EXTRA_REGISTRO, item);
                startActivity(intent);
            }
        });
    }

    private void criaAdapterLista() {
        lvMapa.setAdapter(new ArrayAdapter<Object>(this, 0) {
                              class ViewHolder {
                                  TextView tvNome;
                                  TextView tvEndereco;
                                  TextView tvTelefone;
                                  TextView tvDescricao;
                              }

                              @Override
                              public int getCount() {
                                  if (listMapa != null)
                                      return listMapa.getTotal().intValue();
                                  else
                                      return 0;
                              }

                              @NonNull
                              @Override
                              public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                  ViewHolder viewHolder;
                                  Item item = listMapa.getItems()[position];
                                  //
                                  if (convertView == null) {
                                      convertView = getLayoutInflater().inflate(R.layout.item_listagem, null);
                                      viewHolder = new ViewHolder();
                                      convertView.setTag(viewHolder);
                                      //
                                      viewHolder.tvNome = convertView.findViewById(R.id.tvNome);
                                      viewHolder.tvEndereco = convertView.findViewById(R.id.tvEndereco);
                                      viewHolder.tvTelefone = convertView.findViewById(R.id.tvTelefone);
                                      viewHolder.tvDescricao = convertView.findViewById(R.id.tvDescricao);
                                  } else
                                      viewHolder = (ViewHolder) convertView.getTag();
                                  //
                                  viewHolder.tvNome.setText(item.getData().getNome().getIv());
                                  viewHolder.tvEndereco.setText(item.getData().getEndereco().getIv());
                                  viewHolder.tvTelefone.setText(item.getData().getTelefone().getIv());
                                  viewHolder.tvDescricao.setText(item.getData().getDescricao().getIv());
                                  //
                                  return convertView;
                              }
                          }
        );
    }

    private void carregarWebService() {
        srMapa.setRefreshing(true);
        new WebServiceController().carregaListaMapa(this, new WebServiceController.CarregaListaMapaListener() {
            @Override
            public void onResultOk(Squidex squidex) {
                listMapa = squidex;
                ((ArrayAdapter) lvMapa.getAdapter()).notifyDataSetChanged();
                srMapa.setRefreshing(false);
            }

            @Override
            public void onErro() {
                listMapa = null;
                ((ArrayAdapter) lvMapa.getAdapter()).notifyDataSetChanged();
                srMapa.setRefreshing(false);
            }
        });

    }
}
