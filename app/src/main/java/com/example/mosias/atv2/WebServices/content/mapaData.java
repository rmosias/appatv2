package com.example.mosias.atv2.WebServices.content;

import java.io.Serializable;

public class mapaData implements Serializable
{
    private StringValue nome;
    private StringValue endereco;
    private StringValue telefone;
    private StringValue descricao;
    private StringValue tipo;

    public StringValue getNome() {
        return nome;
    }

    public void setNome(StringValue nome) {
        this.nome = nome;
    }

    public StringValue getEndereco() {
        return endereco;
    }

    public void setEndereco(StringValue endereco) {
        this.endereco = endereco;
    }

    public StringValue getTelefone() {
        return telefone;
    }

    public void setTelefone(StringValue telefone) {
        this.telefone = telefone;
    }

    public StringValue getDescricao() {
        return descricao;
    }

    public void setDescricao(StringValue descricao) {
        this.descricao = descricao;
    }

    public StringValue getTipo() {
        return tipo;
    }

    public void setTipo(StringValue tipo) {
        this.tipo = tipo;
    }
}
