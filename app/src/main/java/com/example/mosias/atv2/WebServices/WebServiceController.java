package com.example.mosias.atv2.WebServices;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import com.example.mosias.atv2.WebServices.content.mapaData;
import com.example.mosias.atv2.WebServices.content.Squidex;
import com.example.mosias.atv2.WebServices.content.Token;

public class WebServiceController {
    /**
     * Responsável por gerenciar as threads que realizam as chamadas web
     */
    private static RequestQueue requestQueue;
    private static Token token;

    public RequestQueue getRequestQueueInstance(Context context)
    {
        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(context);
        return requestQueue;
    }

    public void carregaListaMapa(final Context context
            , final CarregaListaMapaListener carregaListaMapaListener)
    {
        if (token == null)
        {
            geraToken(context, new GeraTokenListener()
            {
                @Override
                public void onTokenOk()
                {
                    carregaListaMapa(context, carregaListaMapaListener);
                }

                @Override
                public void onErro()
                {
                    if (carregaListaMapaListener != null)
                        carregaListaMapaListener.onErro();
                }
            });
        }
        else
        {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    "https://cloud.squidex.io/api/content/atvmapa/mapa",
                    null,
                    new Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            if (carregaListaMapaListener != null)
                                carregaListaMapaListener.onResultOk(new Gson().fromJson(response.toString(), Squidex.class));
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            if (carregaListaMapaListener != null)
                                carregaListaMapaListener.onErro();
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", token.getToken_type() + " " + token.getAccess_token());
                    return headers;
                }
            };
            getRequestQueueInstance(context).add(jsonObjectRequest);
        }
    }

    public void criaMapa(final Context context, final mapaData data, final UpdateMapaListener criaMapaListener) throws JSONException
    {
        if (token == null)
        {
            geraToken(context, new GeraTokenListener()
            {
                @Override
                public void onTokenOk()
                {
                    try
                    {
                        criaMapa(context, data, criaMapaListener);
                    }
                    catch (JSONException e)
                    {
                        if (criaMapaListener != null)
                            criaMapaListener.onErro();
                    }
                }

                @Override
                public void onErro()
                {
                    if (criaMapaListener != null)
                        criaMapaListener.onErro();
                }
            });
        }
        else
        {
            JsonObjectRequest jsonObjectRequest
                    = new JsonObjectRequest(Request.Method.POST,
                    "https://cloud.squidex.io/api/content/atvmapa/mapa?publish=true",
                    new JSONObject(new Gson().toJson(data)),
                    new Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            if (criaMapaListener != null)
                                criaMapaListener.onResultOk();
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            if (criaMapaListener != null)
                                criaMapaListener.onErro();
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", token.getToken_type() + " " + token.getAccess_token());
                    return headers;
                }
            };
            getRequestQueueInstance(context).add(jsonObjectRequest);
        }
    }

    public void atualizaMapa(final Context context, final mapaData data, final String id, final UpdateMapaListener atualizaMapaListener) throws JSONException
    {
        if (token == null)
        {
            geraToken(context, new GeraTokenListener()
            {
                @Override
                public void onTokenOk()
                {
                    try
                    {
                        atualizaMapa(context, data, id, atualizaMapaListener);
                    }
                    catch (JSONException e)
                    {
                        if (atualizaMapaListener != null)
                            atualizaMapaListener.onErro();
                    }
                }

                @Override
                public void onErro()
                {
                    if (atualizaMapaListener != null)
                        atualizaMapaListener.onErro();
                }
            });
        }
        else
        {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT,
                    "https://cloud.squidex.io/api/content/atvmapa/mapa/" + id,
                    new JSONObject(new Gson().toJson(data)),
                    new Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            if (atualizaMapaListener != null)
                                atualizaMapaListener.onResultOk();
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            if (atualizaMapaListener != null)
                                atualizaMapaListener.onErro();
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", token.getToken_type() + " " + token.getAccess_token());
                    return headers;
                }
            };
            getRequestQueueInstance(context).add(jsonObjectRequest);
        }
    }

    public void deletaMapa(final Context context, final String id, final UpdateMapaListener deleteMapaListener)
    {
        if (token == null)
        {
            geraToken(context, new GeraTokenListener()
            {
                @Override
                public void onTokenOk()
                {

                    deletaMapa(context, id, deleteMapaListener);

                }

                @Override
                public void onErro()
                {
                    if (deleteMapaListener != null)
                        deleteMapaListener.onErro();
                }
            });
        }
        else
        {
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE,
                    "https://cloud.squidex.io/api/content/atvmapa/mapa/" + id,
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response)
                        {
                            if (deleteMapaListener != null)
                                deleteMapaListener.onResultOk();
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            if (deleteMapaListener != null)
                                deleteMapaListener.onErro();
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", token.getToken_type() + " " + token.getAccess_token());
                    return headers;
                }
            };
            getRequestQueueInstance(context).add(stringRequest);
        }
    }

    private void geraToken(Context context, final GeraTokenListener geraTokenListener)
    {
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                "https://cloud.squidex.io/identity-server/connect/token",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        token = new Gson().fromJson(response, Token.class);
                        if (geraTokenListener != null)
                            geraTokenListener.onTokenOk();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        if (geraTokenListener != null)
                            geraTokenListener.onErro();
                    }
                }
        )
        {
            @Override
            public String getBodyContentType()
            {
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("grant_type", "client_credentials");
                params.put("client_id", "atvmapa:rafael");
                params.put("client_secret", "AOXJLY5v/MB/CjmScRL5qaz9yf2o0JA6Fxnet40QOrI=");
                params.put("scope", "squidex-api");
                return params;
            }
        };
        //
        getRequestQueueInstance(context).add(stringRequest);
    }


    public interface GeraTokenListener
    {
        public abstract void onTokenOk();

        public abstract void onErro();
    }

    public interface CarregaListaMapaListener
    {
        public abstract void onResultOk(Squidex squidex);

        public abstract void onErro();
    }

    public interface UpdateMapaListener
    {
        public abstract void onResultOk();

        public abstract void onErro();
    }
}